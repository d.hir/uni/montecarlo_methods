///////////////////////////////////////
/// RG.H
/// ====
/// CLASS CREATING (BETTER) RANDOM NUMBERS
/// PROVIDES EASY TO USE FRONTEND FOR STANDARD C++ <RANDOM>
//////////////////////////////////////

#ifndef RG_H
#define RG_H

/* provides random numbers */
#include <random>

class RandomGenerator {
  private:
    std::random_device rd;
    std::mt19937 gen;
    std::uniform_int_distribution<> disint;
    std::uniform_int_distribution<long> dislong;
    std::uniform_real_distribution<> disreal;
    
  public:
    /* standard constructor; not recommended as this does not setup the distributions */
    RandomGenerator();
    ~RandomGenerator();
    /* recommended constructor */
    RandomGenerator(int minint, int maxint, double minreal, double maxreal);
    /* get random integer in interval [minint, maxint] set by constructor */
    int randn();
    /* get random integer in interval [minint, maxint]; a lot slower than randn() */
    long randn(long minint, long maxint);
    /* get random double in interval [minreal, maxreal] set by constructor */
    double rand();
};

#endif
