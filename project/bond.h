///////////////////////////////////////
/// BOND.H
/// ======
/// CLASS FOR OBJECTS WHICH DESCRIBE THE BONDS BETWEEN TWO SITES
//////////////////////////////////////

#ifndef BOND_H
#define BOND_H

/* for using random generator */
#include "rg.h"

/* static random generator; not every bond needs its own generator */
static RandomGenerator* rg = new RandomGenerator(0.0, 1.0, 0, 1);

class Bond {
  private:
    int* x1;  // address of first endpoint
    int* x2;  // address of second endpoint    
    
  public:
    /* occupation status of this bond */
    bool occupied;
    /* constructor for creating a bond between x1_ and x2_ */
    Bond(int* x1_, int* x2_);
    /* update this bond */
    void update(double p);
    
    /* find specific bond in array of bonds */
    static Bond* findBond(Bond* bondlist, int length, int* x1, int* x2);
};


#endif
