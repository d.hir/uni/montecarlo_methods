///////////////////////////////////////
/// RG.CPP
/// ======
/// CLASS FOR SIMULATING A Q-STATE POTTS MODEL USING A MARKOV CHAIN MONTE CARLO
/// METROPOLIS ALGORITHM
//////////////////////////////////////

#include "rg.h"

///////////////////////
// RANDOMGENERATOR::RANDOMGENERATOR
// ------------
// STANDARD CONSTRUCTOR FOR THE RANDOM NUMBER GENERATOR
// THIS IS ONLY RECOMMENDED IF ONE WANTS TO ONLY USE RANDN(INT, INT)
// THIS CONSTRUCTOR DOES NOT ALLOW USAGE OF THE OTHER GENERATORS
//
// IN:
//    -
// OUT:
//    -
///////////////////////
RandomGenerator::RandomGenerator() {
  this->gen = std::mt19937(this->rd());
};

///////////////////////
// RANDOMGENERATOR::RANDOMGENERATOR
// ------------
// RECOMMENDED CONSTRUCTOR FOR THE RANDOM NUMBER GENERATOR
//
// IN:
//    MININT    (INT)     ... LOWER BOUND OF INTEGER RANDOM GENERATOR RANDN()
//    MAXINT    (INT)     ... UPPER BOUND OF INTEGER RANDOM GENERATOR RANDN()
//    MINREAL   (DOUBLE)  ... LOWER BOUND OF DOUBLE RANDOM GENERATOR RAND()
//    MAXREAL   (DOUBLE)  ... UPPER BOUND OF DOUBLE RANDOM GENERATOR RAND()
// OUT:
//    -
///////////////////////
RandomGenerator::RandomGenerator(int minint, int maxint, double minreal, double maxreal) {
  this->gen = std::mt19937(this->rd());
  this->disint = std::uniform_int_distribution<>(minint, maxint);
  this->disreal = std::uniform_real_distribution<>(minreal, maxreal);
};

RandomGenerator::~RandomGenerator() {
  /*delete this->gen;
  delete this->disint;
  delete this->disreal;
  delete this->dislong;*/
};

///////////////////////
// RANDOMGENERATOR::RANDN
// ------------
// RETURNS A RANDOM INTEGER IN THE INTERVAL [MININT, MAXINT] SPECIFIED
// IN THE CONSTRUCTOR
//
// IN:
//    -
// OUT:
//    (INT) RANDOM INTEGER IN INTERVAL [MININT, MAXINT]
///////////////////////
int RandomGenerator::randn() {
  return this->disint(this->gen);
};

///////////////////////
// RANDOMGENERATOR::RANDN
// ------------
// RETURNS A RANDOM INTEGER IN THE INTERVAL [MININT, MAXINT]
//
// IN:
//    MININT    (INT) ... LOWER BOUND OF INTEGER RANDOM GENERATOR
//    MAXINT    (INT) ... UPPER BOUND OF INTEGER RANDOM GENERATOR
// OUT:
//    (LONG) RANDOM INTEGER IN INTERVAL [MININT, MAXINT]
///////////////////////
long RandomGenerator::randn(long minint, long maxint) {
  this->dislong = std::uniform_int_distribution<long>(minint, maxint);
  return this->dislong(this->gen);
};

///////////////////////
// RANDOMGENERATOR::RAND
// ------------
// RETURNS A RANDOM DOUBLE IN THE INTERVAL [MINREAL, MAXREAL] SPECIFIED
// IN THE CONSTRUCTOR
//
// IN:
//    -
// OUT:
//    (DOUBLE) RANDOM DOUBLE IN THE INTERVAL [MINREAL, MAXREAL]
///////////////////////
double RandomGenerator::rand() {
  return this->disreal(this->gen);
};
