#!/usr/bin/env python3

## for calling C++ program
import subprocess
## for numpy arrays
import numpy
## change backend to fix warnings in suppressed mode
import matplotlib
matplotlib.use('svg');
## for plotting results
import matplotlib.pyplot as mpl
import matplotlib.animation as animation
from matplotlib.widgets import Slider
## for generating the uid
import datetime
## for parsing the commandline arguments
import argparse
## for exiting with return value
import sys
## for timing execution
from timer import Timer

## name of the C++ executable
EXEC_NAME = 'project1';

#### Parsing the commandline arguments
parser = argparse.ArgumentParser();
parser.add_argument('-c', '--compile', action='store_true', 
    help="recompile C++ program completely");
parser.add_argument('-r', '--remove', action='store_true', 
    help="remove all directories created for storing results");
parser.add_argument('-s', '--suppress', action='store_true', 
    help="don't show plots after computation");
parser.add_argument('-t', '--temporary', action='store_true', 
    help="delete all files created during computation once finished");
parser.add_argument('-m', '--make', action='store_true', 
    help="just compile C++ program and remove all directories");
parser.add_argument('-d', '--default', action='store_true', 
    help="use C++ default parameters");
parser.add_argument('-g', '--gif', action='store_true',
    help="create a gif file animating the lattice over time");
parser.add_argument('--dimx', default=8, 
    help="number of sites in x-direction (default: 8)");
parser.add_argument('--dimy', default=8, 
    help="number of sites in y-direction (default: 8)");
parser.add_argument('--q', default=2,
    help="number of possible different spins (default: 2)");
parser.add_argument('--runid', default=0,
    help="id of this run (default: time of execution)");
parser.add_argument('--nequi', default=1000,
    help="number of equilibration steps (default: 1000)");
parser.add_argument('--nmeas', default=10000,
    help="number of measurements (default: 10000)");
parser.add_argument('--nskip', default=10,
    help="number of sweeps between measurements (default: 10)");
parser.add_argument('--coldstart', action='store_true',
    help="create a gif file animating the lattice over time");
parser.add_argument('--optimize', default=None,
    help="optimization argument to gcc in Makefile (optional)");

args = parser.parse_args();

T = Timer();
T.start("Total execution time");

#### remove all subdirectories if asked for
if args.remove or args.make:
  print("Removing previous results...");
  T.start("remove directories");
  subprocess.call('make remdirs', shell=True);
  T.stop("remove directories");
  
#### compile the C++ files from scratch if asked for
#### otherwise just compile changes
opt_arg = "";
if args.optimize:
  opt_arg = " OPTIMIZATIONLEVEL=" + str(args.optimize);
if args.compile or args.make:
  print("Compiling...");
  T.start("Compilation");
  ret = subprocess.call("make new" + opt_arg, shell=True);
  T.stop("Compilation");
  if not ret == 0:
    print('Return value: ' + ret);
    sys.exit(1);
else:
  print("Running make to check for changed source code...");
  T.start("Compilation");
  ret = subprocess.call("make" + opt_arg, shell=True);
  T.stop("Compilation");
  if not ret == 0:
    print('Return value: ' + str(ret));
    sys.exit(1);

#### exit if only compilation and directory removal was ordered
if args.make:
  sys.exit(0);

print("Parsing Arguments...");
T.start("Argument parsing");

#### generate UID based on time
if not args.runid:
  dt = datetime.datetime.now()
  run_uid = "{:04d}{:02d}{:02d}{:02d}{:02d}{:02d}".format(dt.year, 
      dt.month, dt.day, dt.hour, dt.minute, dt.second);
else:
  run_uid = str(args.runid);
print("ID = " + run_uid);

#### define simulation parameters
dimx = int(args.dimx);              # (int) number of sites in x direction
dimy = int(args.dimy);              # (int) number of sites in y direction
q = int(args.q);                    # (int) number of possible states 
nequi = int(args.nequi);            # (int) number of equilibration sweeps
nmeas = int(args.nmeas);            # (int) number of measurements per result
nskip = int(args.nskip);            # (int) number of skipped sweeps
hotstart = int(not args.coldstart); # (bool as int) randomize each site

#### create list of arguments for passing to C++ program
cppargs = [str(run_uid)];
if not args.default:
  cppargs += [str(dimx), str(dimy), str(q), str(nequi), 
    str(nmeas), str(nskip), str(hotstart)];

T.stop("Argument parsing");

#### call the C++ program, capture the output from stdout and
#### split each line of the output in separate entries in matrix 'res'
#### manually pipe stderr of c++ program to stdout of this one
print("Starting Simulation...");
T.start("Simulation");
processHandle = subprocess.run(["./" + EXEC_NAME] + cppargs, \
    capture_output=True);
T.stop("Simulation");
res = [j.split() for j in processHandle.stdout.splitlines()];
if processHandle.stderr:
  errmsg = processHandle.stderr.splitlines();
  print('\n'.join(['<STDERR:> ' + str(j)[2:-1] for j in errmsg]));

#### extract parameters returned from C++ program
#### this is just used for confirming correct parameters
print("Parsing results...");
T.start("Result interpretation");
if not int(res[0][1]) == nequi: print("<WARNING:> Mismatching nequi");
if not int(res[1][1]) == nmeas: print("<WARNING:> Mismatching nmeas");
if not int(res[2][1]) == nskip: print("<WARNING:> Mismatching nskip");
jc = float(res[3][1]);  ## critical temperature in form of parameter J
dirname = str(res[4][1])[2:-1];
clustermethod = bool(int(res[5][1]));
if not (q == 0) == clustermethod: print("<WARNING:> Mismatching method");

#### extract name of calculated observables
observable_names = [str(j)[2:-1] for j in res[6]];

#### convert values in string list 'res' to useful numpy.array datatype
data = numpy.array(res[7:], dtype = float).transpose();

#### all values of J are printed first by C++ program
J = data[0];

T.stop("Result interpretation");

#### plot all observables with uncertainty and a line at jc and save plots
print("Plotting observables...");
T.start("Plotting");
for j in range(1, len(observable_names), 2):
  mpl.figure(figsize=(6, 9));
  mpl.errorbar(J, data[j], data[j + 1], ls='', c='b');
  # if numpy.mean(data[j+1] / numpy.abs(numpy.mean(data[j]))) < 0.02:
  mpl.plot(J, data[j], 'o', ls='', mec='b', mfc='none');
  mpl.plot([jc, jc], [min(data[j] - data[j + 1]), max(data[j] + 
      data[j + 1])], 'm--', linewidth = .5);
  mpl.xlabel('J');
  mpl.ylabel(observable_names[j] + "(J)");
  mpl.title(observable_names[j] + "(J)");
  mpl.savefig(dirname + "/" + observable_names[j] + ".svg");
  mpl.savefig(dirname + "/" + observable_names[j] + ".png");
T.stop("Plotting");

#### read files stored by C++ program and store results in list
#### each file contains a spin configuration at the specified J value
#### at the end the list gets converted to numpy.array
if args.gif or not args.suppress:
  print("Loading result files...");
  T.start("File loading");
  tmp_data = []
  for j in J:
    strj = 'p';
    if j < 0:
      strj = 'm';
    strj += "{:1.2f}".format(abs(j));
    with open(dirname + "/S_J" + strj + ".csv") as file:
      tmp_data.append([j[:-1].split(',') for j in file.readlines()]);
  resmat = numpy.array(tmp_data, dtype=int);
  T.stop("File loading");

#### create figure for spin-configurations with a slider for changing J
if not args.suppress:
  print("Plotting lattice with slider...");
  T.start("Slider plot");
  fig = mpl.figure(figsize=(5, 6));
  ax = mpl.subplot(111);
  fig.subplots_adjust(bottom=0.25);
  img = ax.matshow(resmat[0]);
  ax = fig.add_axes([0.15, 0.1, 0.65, 0.03])
  dJ = J[1] - J[0];
  slider = Slider(ax, 'J', J[0], J[-1], valinit=jc, valfmt='%1.2f', valstep=dJ)
  T.stop("Slider plot");
  def update(val):
    wantedj = float(slider.val);
    indexj = int((wantedj - J[0]) / dJ);
    img.set_data(resmat[indexj]);
    fig.canvas.draw();
  slider.on_changed(update);

#### create figure for plotting the spin-configurations as a looping gif
if args.gif:
  print("Creating animated gif...");
  T.start("GIF");
  giffig = mpl.figure(figsize=(5, 5));
  images = [];
  for j in range(len(J)):
    tmp_img = mpl.imshow(resmat[j]);
    images.append([tmp_img]);
  ani = animation.ArtistAnimation(giffig, images, interval=3e4/len(J), \
      blit=False);
  ani.save(dirname + "/S.gif", writer='imagemagick');
  T.stop("GIF");

#### remove directory created in this run
if args.temporary:
  print("Removing traces...");
  subprocess.call('rm -rf ' + dirname + '/', shell=True);

T.stop("Total execution time");

#### show plots if not explicitly asked to suppress the output
if not args.suppress:
  mpl.show();
