/////////////////////////////////////////
/// SETTINGS.H
/// ==========
/// COLLECTION OF SETTINGS AND DEFAULT VALUES
/////////////////////////////////////////

#ifndef SETTINGS_H
#define SETTINGS_H

/* system settings */
#define ID_LENGTH 14                // length of UID
#define STRING_BUFFER_LENGTH 80     // max length of folder name

/* default values */
#define DEFAULT_DIM_X 8             // int:  number of sites in x direction
#define DEFAULT_DIM_Y 8             // int:  number of sites in y direction
#define DEFAULT_Q_VAL 2             // int:  number of possible spin states
#define DEFAULT_N_EQUI 1000         // int:  number of equilibration steps
#define DEFAULT_N_MEAS 1000         // int:  number of measurements per result
#define DEFAULT_N_SKIP 10           // int:  number of sweeps not measured
#define DEFAULT_USE_HOTSTART true   // bool: randomize starting configuration
#define DEFAULT_USE_CLUSTER true    // bool: use Swendsen-Wang algorithm 

/* simulation settings */
#define MAGNETIC_FIELD_DIRECTION 1  // direction of the external magnetic field
#define J_STEP 0.01                 // stepsize for J
#define J_NUM 200                   // number of different J values

#endif
