import time
import numpy

class Timepoint:
  NS_PER_ACCURACY = 1;
  US_PER_ACCURACY = 1000 * NS_PER_ACCURACY;
  MS_PER_ACCURACY = 1000 * US_PER_ACCURACY;
  S_PER_ACCURACY = 1000 * MS_PER_ACCURACY;
  MINS_PER_ACCURACY = 60 * S_PER_ACCURACY;
  HRS_PER_ACCURACY = 60 * MINS_PER_ACCURACY;
  DAYS_PER_ACCURACY = 24 * HRS_PER_ACCURACY;
  STR_LIST = ["days", "hours", "mins", "secs", "ms", "us", "ns"];
  
  def __init__(self, str_id):
    self.id = str_id;
    self.begin = time.perf_counter_ns();
    self.end = 0.0;
    self.duration = 0.0;
  
  def __dest__(self):
    if self.end == 0.0:
      print(self);
    
  def setEnd(self):
    self.end = time.perf_counter_ns();
    self.duration = self.end - self.begin;
    
  def __str__(self):
    if self.duration == 0.0:
      if self.end == 0.0:
        self.setEnd();
      else:
        self.duration = self.end - self.begin;
    r = Timepoint.split(self.duration);
    still_zero = True;
    printstr = "TIMER <" + self.id + ">: ";
    for i, ri in enumerate(r):
      if ri != 0 or not still_zero:
        still_zero = False;
        printstr += str(ri) + " " + Timepoint.STR_LIST[i] + " ";
    return printstr;
  
  def extract(input_val, MODULUS):
    val = int(input_val // MODULUS);
    remainder = input_val % MODULUS;
    return val, remainder;
  
  def split(time_ns):
    d, time_ns = Timepoint.extract(time_ns, Timepoint.DAYS_PER_ACCURACY);
    h, time_ns = Timepoint.extract(time_ns, Timepoint.HRS_PER_ACCURACY);
    m, time_ns = Timepoint.extract(time_ns, Timepoint.MINS_PER_ACCURACY);
    s, time_ns = Timepoint.extract(time_ns, Timepoint.S_PER_ACCURACY);
    ms, time_ns = Timepoint.extract(time_ns, Timepoint.MS_PER_ACCURACY);
    us, time_ns = Timepoint.extract(time_ns, Timepoint.US_PER_ACCURACY);
    ns, time_ns = Timepoint.extract(time_ns, Timepoint.NS_PER_ACCURACY);
    return d, h, m, s, ms, us, ns;
    
class Timer:
  def __init__(self):
    self.timing_counter = -1;
    self.timings = dict();
  
  def start(self, timing_id = ""):
    self.timing_counter += 1;
    if timing_id == "":
      timing_id = "Unnamed Timing " + str(self.timing_counter);
    self.timings[timing_id] = Timepoint(timing_id);
  
  def stop(self, timing_id = "Unnamed Timing", print_results = True):
    if timing_id == "Unnamed Timing":
      timing_id += " " + str(timing_counter);
    if not timing_id in self.timings:
      print("Timer not yet started! ID: " + timing_id);
      return;
    self.timings[timing_id].setEnd();
    if (print_results):
      self.print(timing_id);
    
  def print(self, timing_id = ""):
    if timing_id == "":
      for tp_id, tp in self.timings.items():
        print(tp);
    elif timing_id in self.timings:
      print(self.timings[timing_id]);
    else:
      print("Timer not yet started! ID: " + timing_id);      
  
  
