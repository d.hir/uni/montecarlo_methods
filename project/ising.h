///////////////////////////////////////
/// ISING.H
/// ====
/// CLASS FOR SIMULATING AN ISING MODEL USING A MARKOV CHAIN MONTE CARLO
/// SWENDSEN-WANG CLUSTER ALGORITHM
//////////////////////////////////////

#ifndef ISING_H
#define ISING_H

/* Ising class is derived from PottsMonteCarlo class */
#include "mc.h"
/* for using 'Bond' objects */
#include "bond.h"

class Ising : public PottsMonteCarlo {
  private:
    Bond* bonds;      // pointer to array of bonds between lattice points
    double p;         // probability for creating an allowed bond
    Bond** adjacent;  // neighbour array with pointers to adjacent bonds
    
    /* recursive method to get all elements of a single cluster */
    void recursiveColoring(int site_index, int current_color, int* color, 
        std::vector<int> &cluster);
    /* get a vector of all clusters */
    std::vector<std::vector<int>> getClusters();
    /* update all bonds to the current spin configuration */
    void updateBonds();
    /* do a single sweep = update each cluster once */
    void singleSweep();
        
    /* print a single bond, used by printBonds() method */
    static void printbond(bool vertical, bool isbond);
  public:
    /* create Ising model of size dim1_ * dim2_ */
    Ising(int dim1_, int dim2_);
    /* destructor for free-ing allocated space on heap */
    ~Ising();
    /* set J, M, Mdir for this simulation; overrides PottsMonteCarlo method */
    void setEnergyParameter(double J_, double M_, int Mdir_);
    
    /* print lattice with bonds, used mainly for debugging */
    void printBonds();
    /* print clusters in lattice, used mainly for debugging */
    void printClusters(int* color);

};


#endif
