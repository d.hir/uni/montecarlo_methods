///////////////////////////////////////
/// ISING.CPP
/// ======
/// IMPLEMENTATION OF ISING.H
/// CLASS FOR SIMULATING AN ISING MODEL USING A MARKOV CHAIN MONTE CARLO
/// SWENDSEN-WANG CLUSTER ALGORITHM
//////////////////////////////////////

#include "ising.h"

/* for using 'Bond' objects */
#include "bond.h"
/* for writing to stdout */
#include <iostream>
/* for exp */
#include <cmath>
/* for easier handling of dynamic arrays */
#include <vector>

///////////////////////
// ISING::ISING
// ------------
// CONSTRUCTOR FOR GENERATING A MONTE CARLO SIMULATION
//
// IN:
//    DIM1_   (INT)   ... NUMBER OF SITES IN X-DIRECTION
//    DIM2_   (INT)   ... NUMBER OF SITES IN Y-DIRECTION
// OUT:
//    -
///////////////////////
Ising::Ising(int dim1_, int dim2_) : PottsMonteCarlo(dim1_, dim2_, 2) {
  /* calculate probability for creating an allowed bond */
  this->p = 1 - exp(-this->J);
  /* allocate space for bond array and instantiate each one */
  this->bonds = (Bond*) malloc(sizeof(Bond) * this->N * 2);
  for (int j = 0; j < this->N * 2; j++)
    this->bonds[j] = Bond(&this->S[j / 2], &this->S[this->neib[j * 2 - j % 2]]);
  /* allocate and instantiate neighbour array for adjacent bonds */
  this->adjacent = (Bond**) malloc(sizeof(Bond*) * this->N * 4);
  for (int j = 0; j < this->N * 4; j++) {
    this->adjacent[j] = Bond::findBond(this->bonds, this->N * 2, 
        &this->S[j / 4], &this->S[this->neib[j]]);
  }
};

///////////////////////
// ISING::~ISING
// ------------
// DESTRUCTOR FOR FREE-ING ALLOCATED SPACE ON HEAP
//
// IN:
//    -
// OUT:
//    -
///////////////////////
Ising::~Ising() {
  free(this->bonds);
  free(this->adjacent);
};

///////////////////////
// ISING::UPDATEBONDS
// ------------
// UPDATE EACH BOND TO THE CHANGED SPINS ON THE LATTICE
//
// IN:
//    -
// OUT:
//    -
///////////////////////
void Ising::updateBonds() {
  for (int j = 0; j < this->N * 2; j++)
    bonds[j].update(this->p);
};

///////////////////////
// ISING::PRINTBOND
// ------------
// PRINT A SINGLE BOND IF ACTIVE
//
// IN:
//    VERTICAL  (BOOL)  ... IF BOND IS VERTICAL (HORIZONTAL OTHERWISE)
//    ISBOND    (BOOL)  ... IF BOND IS ACTIVE
// OUT:
//    -
///////////////////////
void Ising::printbond (bool vertical, bool isbond) {
  char bondver = ':';
  char bondhor = '-';
  if (isbond)
    if (vertical)
      std::cout << bondver;
    else
      std::cout << bondhor << bondhor << bondhor;
  else
    if (vertical)
      std::cout << " ";
    else
      std::cout << "   ";
};

///////////////////////
// ISING::PRINTBONDS
// ------------
// PRINT CURRENT LATTICE AND BONDS
//
// IN:
//    -
// OUT:
//    -
///////////////////////
void Ising::printBonds() {
  std::cout << "p = " << p << std::endl;
  for (int j = 0; j < this->dim1; j++) {
    std::cout << " ";
    for (int k = 0; k < this->dim2; k++) {
      std::cout << this->S[j * dim2 + k];
      Ising::printbond(false, this->bonds[2 * (j * dim2 + k)].occupied);
    }
    std::cout << "\n ";
    for (int k = 0; k < this->dim2; k++) {
      Ising::printbond(true, this->bonds[2 * (j * dim2 + k) + 1].occupied);
      std::cout << "   ";
    }
    if (j != this->dim1)
      std::cout << std::endl;
  }
};

///////////////////////
// ISING::SETENERGYPARAMETER
// ------------
// SET J AND M FOR THIS SIMULATION
// THIS OVERRIDES POTTSMONTECARLO::SETENERGYPARAMETER
//
// IN:
//    J_     (DOUBLE) ... VALUE FOR NEIGHBOUR COUPLING CONSTANT
//    M_     (DOUBLE) ... VALUE FOR MAGNETIZATION COUPLING CONSTANT
//    MDIR_  (INT)    ... DIRECTION OF THE EXTERNAL MAGNETIC FIELD
// OUT:
//    -
///////////////////////
void Ising::setEnergyParameter(double J_, double M_, int Mdir_) {
  this->J = J_;
  this->M = M_;
  this->Mdir = Mdir_;
  this->p = 1 - exp(-this->J);
};

///////////////////////
// ISING::RECURSIVECOLORING
// ------------
// MARK ALL ELEMENTS IN SAME CLUSTER AS SITE_INDEX WITH CURRENT_COLOR
// AND ADD THEM TO CLUSTER VECTOR
//
// IN:
//    SITE_INDEX    (INT)         ... INDEX OF SITE OF INTEREST
//    CURRENT_COLOR (INT)         ... COLOR OF THE CURRENT CLUSTER
//    COLOR         (INT*)        ... ARRAY OF COLOR OF EACH SITE
//    CLUSTER       (VECTOR<INT>) ... VECTOR OF ALL SITES IN THIS CLUSTER
// OUT:
//    -
///////////////////////
void Ising::recursiveColoring(int site_index, int current_color, int* color, 
    std::vector<int> &cluster) {
  color[site_index] = current_color;
  /* add current site to cluster */
  cluster.push_back(site_index);
  /* iterate through all neighbouring sites and call recursively if connected */
  for (int j = site_index * 4; j < (site_index + 1) * 4; j++)
    if (this->adjacent[j]->occupied && color[this->neib[j]] == 0)
      Ising::recursiveColoring(this->neib[j], current_color, color, cluster);
}; 

///////////////////////
// ISING::GETCLUSTERS
// ------------
// GET VECTOR OF ALL CLUSTERS
//
// IN:
//    -
// OUT:
//    (VECTOR<VECTOR<INT>>) VECTOR OF CLUSTERS, VECTOR OF VECTOR OF SITE INDICES
///////////////////////
std::vector<std::vector<int>> Ising::getClusters() {
  std::vector<std::vector<int>> clusters;
  /* array storing color of each site */
  int* color = new int[this->N];
  /* start with color 1 for first cluster */
  int current_color = 1;
  /* look through every site on the lattice */
  for (int j = 0; j < this->N; j++) {
    /* skip if already colored */
    if (color[j] != 0)
      continue;
    std::vector<int> cluster;
    /* find all elements in current cluster */
    Ising::recursiveColoring(j, current_color, color, cluster);
    /* add current cluster to vector of clusters */
    clusters.push_back(cluster);
    /* move onward to next cluster, next color */
    current_color++;
  }
  delete[] color;
  return clusters;
};

///////////////////////
// ISING::PRINTCLUSTERS
// ------------
// PRINT CLUSTER FOR EACH ELEMENT OF THE LATTICE
//
// IN:
//    COLOR   (INT*) ... ARRAY OF COLORS FOR EACH ELEMENT
// OUT:
//    -
///////////////////////
void Ising::printClusters(int* color) {
  for (int j = 0; j < this->dim1; j++) {
    for (int k = 0; k < this->dim2; k++)
      std::cout << "   " << color[j * dim2 + k];
    std::cout << "\n" << std::endl;
  }
};

///////////////////////
// ISING::SINGLESWEEP
// ------------
// UPDATE SPINS OF ALL CLUSTERS ONCE
// THIS OVERRIDES POTTSMONTECARLO::SINGLESWEEP
//
// IN:
//    -
// OUT:
//    -
///////////////////////
void Ising::singleSweep() {
  /* first update bonds to be in accordance with the current lattice state */
  this->updateBonds();
  /* get all the clusters of the current state */
  std::vector<std::vector<int>> clusters = Ising::getClusters();
  /* iterate through all clusters */
  for (auto& cluster : clusters) {
    /* with a probability of 0.5 change all spins to 1 (0 otherwise) */
    double r = rg->rand();
    int newval = 0;
    if (r < .5)
      newval = 1;
    /* set each element of this cluster to the new spin value */
    for (auto& ind : cluster)
      this->S[ind] = newval;
  }
};
