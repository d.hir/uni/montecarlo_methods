///////////////////////////////////////
/// HELPER.CPP
/// ==========
/// IMPLEMENTATION OF HELPER.H
/// STATIC CLASS WHICH ACTS AS COLLECTION OF VARIOUS FUNCTIONS
//////////////////////////////////////

#include "helper.h"

#include <iostream>
/* for writing to stdout */
#include <fstream>
/* for writing to files */
#include <cstdlib>
/* for sprintf, atoi */
#include <cmath>
/* for pow, abs, log */
#include <algorithm>
#include "settings.h"
/* for accessing settings and parameters */
#include "rg.h"
/* for generating random UID */
#include "observable.h"
/* for using the 'Observable' struct */
#include "result.h"

///////////////////////
// HELPER::MEAN
// ------------
// CALCULATE MEAN VALUE OF 'A' OF LENGTH 'LENGTH'
//
// IN:
//    A       (DOUBLE*) ... POINTER TO VALUES FOR MEAN CALCULATION 
//    LENGTH  (INT)     ... NUMBER OF ENTRIES AT 'A'
// OUT:
//    (DOUBLE) MEAN OF VARIABLES STORED AT 'A'
///////////////////////
double Helper::mean (double* A, int length) {
  double sum = 0;
  for (int j = 0; j < length; j++)
    sum += A[j];
  return sum / length;
};

///////////////////////
// HELPER::SHIFTSQUARE
// ------------
// CALCULATE A[i] * A[i + SHIFT] FOR ALL i AND STORE RESULT IN 'RES'
//
// IN:
//    RES     (DOUBLE*) ... POINTER TO SPACE FOR STORING RESULTS
//    A       (DOUBLE*) ... POINTER TO VALUES FOR MEAN CALCULATION 
//    LENGTH  (INT)     ... NUMBER OF ENTRIES AT 'A'
//    SHIFT   (INT)     ... SHIFT FOR CALCULATING SHIFTED SQUARE
// OUT:
//    -
///////////////////////
void Helper::shiftsquare (double* res, double* A, int length, int shift) {
  for (int j = 0; j < (length - shift); j++)
    res[j] = A[j] * A[j + shift];
};

///////////////////////
// HELPER::GETAUTOCORRELATION
// ------------
// CALCULATE UNCERTAINTY OF MEASUREMENTS 'X' USING AUTOCORRELATION
//
// IN:
//    X       (DOUBLE*) ... POINTER TO VALUES FOR CALCULATION 
//    NMEAS   (INT)     ... NUMBER OF ENTRIES AT 'X'
// OUT:
//    (DOUBLE) UNCERTAINTY OF MEAN BY AUTOCORRELATION OF VALUES AT 'X'
///////////////////////
double Helper::getAutocorrelation (double* x, int nmeas) {
  /* autocorrelation function declaration */
  double C[nmeas];
  /* temporary variable for storing numerator */
  double sumtt = 0;
  /* temporary variable for storing denominator */
  double sumlnct = 0;
  /* square value of mean value of x */
  double meanxi2 = Helper::mean(x, nmeas) * Helper::mean(x, nmeas);
  /* temporary storage space for shifted square of x */
  double shsqr[nmeas];
  /* loop over all values of x */
  for (int t = 0; t < nmeas; t++) {
    /* store x[i] * x[i + t] in 'shsqr' */
    Helper::shiftsquare(shsqr, x, nmeas, t);
    /* calculate autocorrelation function at shift 't' */
    C[t] = Helper::mean(shsqr, nmeas - t) - meanxi2;
    /* calculate numerator at point 't' */
    sumtt += t*t;
    /* stop if C[t] is too small before applying logarithm */
    if (C[t] < 1e-6)
      continue;
    /* calculate denominator at point t */
    sumlnct += log(C[t] / C[0]) * t;
  }
  /* calculate autocorrelation time */
  double autocorrelationtime = - sumtt / sumlnct;
  /* calculate and return uncertainty epsilon */
  return sqrt(2 * abs(autocorrelationtime)) / nmeas;
};

///////////////////////
// HELPER::MEANEXCEPT
// ------------
// CALCULATE MEAN VALUE OF 'A' OF LENGTH 'LENGTH' OMITTING INDEX 'EXCEPTION'
//
// IN:
//    X         (DOUBLE*) ... POINTER TO VALUES FOR MEAN CALCULATION 
//    LENGTH    (INT)     ... NUMBER OF ENTRIES AT 'A'
//    EXCEPTION (INT)     ... INDEX OF ELEMENT TO IGNORE
// OUT:
//    (DOUBLE) MEAN OF VARIABLES STORED AT 'A' WITHOUT 'EXCEPTION'
///////////////////////
double Helper::meanExcept(double* x, int length, int exception) {
  double sum = 0;
  for (int j = 0; j < exception; j++)
    sum += x[j];
  for (int j = exception + 1; j < length; j++)
    sum += x[j];
  return sum / (length - 1);
};

///////////////////////
// HELPER::GETGENERALJACKKNIFEDEV
// ------------
// CALCULATE UNCERTAINTY OF OBSERVABLE USING REMOVE-1-JACKKNIFE METHOD
//
// IN:
//    OBSERVABLEFUNCTIONEXCEPT (FCT*)    ... OBSERVABLE FOR UNCERTAINTY
//    X                        (DOUBLE*) ... POINTER TO DATA POINTS 
//    LENGTH                   (INT)     ... NUMBER OF ENTRIES AT 'A'
// OUT:
//    (DOUBLE) STANDARD DEVIATION OF OBSERVABLE
///////////////////////
double Helper::getGeneralJackknifeDev(double (*ObservableFunctionExcept)(double*, int, int), double* x, int length) {
  double* exceptionArray = new double[length];
  for (int j = 0; j < length; j++)
    exceptionArray[j] = ObservableFunctionExcept(x, length, j);
  /* calculating observables with each element missing once */
  double exceptionMean = Helper::mean(exceptionArray, length);
  /* calculate mean of those measurements */
  double var = 0;
  for (int j = 0; j < length; j++)
    var += (exceptionMean - exceptionArray[j]) * 
        (exceptionMean - exceptionArray[j]);
  /* calculate the variance of the observable */
  delete[] exceptionArray;
  /* return standard deviation */
  return sqrt((length - 1.0) / length * var);
};

///////////////////////
// HELPER::PRINTRESULTS
// ------------
// PRINT MEAN AND UNCERTAINTY OF ALL OBERVABLES TO STDOUT
//
// IN:
//    R       (RESULT*) ... RESULT ARRAY TO BE PRINTED
//    LENGTH  (INT)     ... NUMBER OF ELEMENTS IN 'R'
// OUT:
//    -
///////////////////////
void Helper::printResults(Result R[J_NUM], int length) {
  for (int k = 0; k < length; k++) {
    std::cout << R[k].j << " ";
    std::cout << R[k].Energy.val << " " << R[k].Energy.dev << " ";
    std::cout << R[k].Magnetization.val << " " << R[k].Magnetization.dev << " ";
    std::cout << R[k].CV.val << " " << R[k].CV.dev << std::endl;
  }
};

///////////////////////
// HELPER::SETOBSERVABLE
// ------------
// CALCULATE MEAN AND UNCERTAINTY OF MEASUREMENT AND STORE RESULTS IN
// OBSERVABLE (OBSERVABLE.VAL FOR MEAN, OBSERVABLE.DEV FOR UNCERTAINTY)
//
// IN:
//    O       (OBSERVABLE) ... OBSERVABLE FOR STORING RESULTS
//    O_VEC   (DOUBLE*)    ... POINTER TO VALUES FOR CONVERSION TO OBSERVABLE 
//    NMEAS   (INT)        ... NUMBER OF ENTRIES AT 'O_VEC'
// OUT:
//    -
///////////////////////
void Helper::setObservable(Observable &O, double* O_vec, int nmeas) {
  O.val = Helper::mean(O_vec, nmeas);
  O.dev = Helper::getGeneralJackknifeDev(&Helper::meanExcept, O_vec, nmeas);
  O.dev *= Helper::getAutocorrelation(O_vec, nmeas); 
};

///////////////////////
// HELPER::STOREOBSERVABLES
// ------------
// STORE OBSERVABLES IN FILE
//
// IN:
//    R                 (RESULT*)  ... ARRAY OF RESULTS TO BE STORED
//    NUM_VALS          (INT)      ... NUMBER OF ENTRIES IN 'R'
//    FOLDERNAME_LENGTH (INT)      ... LENGTH OF FOLDERNAME
//    FOLDERNAME        (CHAR*)    ... NAME OF FOLDER WHERE TO STORE OBSERVABLES
// OUT:
//    -
///////////////////////
void Helper::storeObservables(Result* R, int num_vals, int foldername_length, 
    char* foldername) {
  std::ofstream file;
  char filepath[2 * foldername_length];
  sprintf(filepath, "%s/observables.csv", foldername);
  file.open(filepath);
  if (!file.is_open()) {
    std::cerr << "Could not open file";
    return;
  }
  file << "  J        <E> +- dE        <M> +- dM        <C> +- dC   \n";
  for (int k = 0; k < num_vals; k++) {
    file << R[k].j << ", " << R[k].Energy.val << " " << R[k].Energy.dev << ", "
         << R[k].Magnetization.val << " " << R[k].Magnetization.dev << ", " 
         << R[k].CV.val << " " << R[k].CV.dev << std::endl;
  }
};

///////////////////////
// HELPER::PARSEINPUTS
// ------------
// PARSE INPUT VECTOR 'ARGV' AND STORE RESULTS IN CORRESPONDING VARIABLES
// ALL INPUTS EXCEPT 'ARGC' AND 'ARGV' ARE JUST EMPTY VARIABLES FOR STORAGE
//
// IN:
//    ARGC        (INT)     ... NUMBER OF ARGUMENTS 
//    ARGV        (CHAR**)  ... ARRAY OF CHAR* REPRESENTING INPUT ARGUMENTS
//    DIMX        (INT)     ... NUMBER OF SITES IN X DIRECTION
//    DIMY        (INT)     ... NUMBER OF SITES IN Y DIRECTION
//    Q           (INT)     ... NUMBER OF POSSIBLE SPIN VALUES
//    NEQUI       (INT)     ... NUMBER OF EQUILIBRATION SWEEPS
//    NMEAS       (INT)     ... NUMBER OF MEASUREMENTS PER RESULT
//    NSKIP       (INT)     ... NUMBER OF SWEEPS IN BETWEEN MEASUREMENTS
//    HOTSTART    (BOOL)    ... USE RANDOM STARTING CONFIGURATION FOR EACH SPIN
//    RUN_UID     (LONG)    ... UID USED FOR THIS RUN, RANDOMIZED IF NOT GIVEN
//    FOLDERNAME  (CHAR*)   ... POINTER TO SPACE FOR STORING NAME OF FOLDER
//    CLUSTERMETHOD (BOOL)  ... USE SWENDSEN-WANG CLUSTER ALGORITHM
// OUT:
//    -
///////////////////////
void Helper::parseInputs(int argc, char** argv, int &dimx, int &dimy, int &q, 
    int &nequi, int &nmeas, int &nskip, bool &hotstart, long &run_uid, 
    char foldername[STRING_BUFFER_LENGTH], bool &clustermethod) {
  if (argc < 2) {
    /* generate random UID for this run */
    RandomGenerator* rg = new RandomGenerator();
    run_uid = rg->randn((long) 0, (long) pow(10, ID_LENGTH) - 1);
    delete rg;
  } else {
    run_uid = atol(argv[1]);
  } if (argc >= 5) {
    dimx = atoi(argv[2]);
    dimy = atoi(argv[3]);
    q = atoi(argv[4]);
  } if (argc >= 8) {
    nequi = atoi(argv[5]);
    nmeas = atoi(argv[6]);
    nskip = atoi(argv[7]);
  } if (argc > 8) {
    hotstart = (bool) atoi(argv[8]);
  } 
  
  /* check whether to use Swendsen-Wang or single spin flip algorithm */
  if (q == 0) {
    q = 2;
    clustermethod = true;
  } else 
    clustermethod = false;
  
  /* convert (bool) hotstart to string for foldername */
  char hotstartstring[6];
  if (hotstart)
    sprintf(hotstartstring, "hot");
  else
    sprintf(hotstartstring, "cold");
  
  /* define algorithm name for writing to foldername */
  char clusterstring[9];
  if (clustermethod)
    sprintf(clusterstring, "cluster");
  else
    sprintf(clusterstring, "single");
  
  /* create name of folder and store name at 'foldername' */
  char cmd[STRING_BUFFER_LENGTH + 6];
  sprintf(foldername, "./results/CPP_%014ld_%dx%d_q%d_%d_%d_%d_%s_%s", run_uid, 
      dimx, dimy, q, nequi, nmeas, nskip, hotstartstring, clusterstring);
  /* create directory with name '*foldername' */
  sprintf(cmd, "mkdir -p %s", foldername);
  int success_state = system(cmd);
  if (success_state != 0)
    std::cout << "ERROR: FOLDER-CREATION FAILED" << std::endl;
};

///////////////////////
// HELPER::DELTA
// ------------
// CALCULATE KRONECKER-DELTA FOR A AND B
//
// IN:
//    A       (int)   ... input 1
//    B       (int)   ... input 2
// OUT:
//    (INT) 1 IF A == B, 0 IF A != B
///////////////////////
int Helper::delta(const int a, const int b) {
  if (a == b)
    return 1;
  else
    return 0;
};

///////////////////////
// HELPER::CVEXCEPT
// ------------
// CALCULATE HEAT CAPACITY OMITTING INDEX 'EXCEPTION'
//
// IN:
//    ENERGY    (DOUBLE*) ... POINTER TO ENERGY VALUES
//    LENGTH    (INT)     ... NUMBER OF 'ENERGY' VALUES
//    EXCEPTION (INT)     ... INDEX TO OMIT
// OUT:
//    (DOUBLE) HEAT CAPACITY OMITTING INDEX 'EXCEPTION'
///////////////////////
double Helper::CVExcept(double* energy, int length, int exception) {
  double sqrenergy[length];
  Helper::shiftsquare(sqrenergy, energy, length, 0);
  /* CV = <E^2> - <E>^2 */
  return (Helper::meanExcept(sqrenergy, length, exception) - 
      pow(Helper::meanExcept(energy, length, exception), 2));  
};

///////////////////////
// HELPER::SETHEATCAPACITY
// ------------
// CALCULATE THE MEAN AND UNCERTAINTY OF THE HEAT CAPACITY
//
// IN:
//    R       (RESULT)  ... RESULT STRUCT OF MEASUREMENT
//    ENERGY  (DOUBLE*) ... ARRAY OF ENERGY VALUES USED FOR THIS MEASUREMENT
//    LENGTH  (INT)     ... NUMBER OF ENTRIES AT 'A'
// OUT:
//    -
///////////////////////
void Helper::setHeatCapacity(Result &R, double* energy, int length) {
  double sqrenergy[length];
  Helper::shiftsquare(sqrenergy, energy, length, 0);
  /* CV = <E^2> - <E>^2 */
  R.CV.val = (Helper::mean(sqrenergy, length) - R.Energy.val * R.Energy.val);
  R.CV.dev = Helper::getGeneralJackknifeDev(&Helper::CVExcept, energy, length);
};
