///////////////////////////////////////
/// BOND.CPP
/// ======
/// IMPLEMENTATION OF BOND.H
/// CLASS FOR OBJECTS WHICH DESCRIBE THE BONDS BETWEEN TWO SITES
//////////////////////////////////////

#include "bond.h"

/* for using the random generator */
#include "rg.h"
/* for writing to stdout */
#include <iostream>

///////////////////////
// BOND::BOND
// ------------
// CONSTRUCTOR FOR GENERATING A BOND BETWEEN SITE X1_ AND X2_
//
// IN:
//    X1_   (INT*)   ... ADDRESS OF SITE OF FIRST END
//    X2_   (INT*)   ... ADDRESS OF SITE OF SECOND END
// OUT:
//    -
///////////////////////
Bond::Bond(int* x1_, int* x2_) {
  this->x1 = x1_;
  this->x2 = x2_;
};

///////////////////////
// BOND::UPDATE
// ------------
// UPDATE THIS BOND TO NEW SPINS AT REGISTERED SITES X1 AND X2
//
// IN:
//    P   (DOUBLE)   ... PROBABILITY FOR ACCEPTING VALID BOND
// OUT:
//    -
///////////////////////
void Bond::update(double p) {
  if (*this->x1 == *this->x2) {
    double r = rg->rand();
    if (r < p)
      this->occupied = true;
    else
      this->occupied = false;
  } else
    this->occupied = false;
};

///////////////////////
// BOND::FINDBOND
// ------------
// FIND BOND WITH ENDPOINTS X1, X2 IN ARRAY BONDLIST
//
// IN:
//    BONDLIST   (BOND*)   ... ARRAY OF BONDS
//    LENGTH     (INT)     ... LENGTH OF BONDLIST ARRAY
//    X1         (INT*)    ... ADDRESS OF ONE ENDPOINT
//    X2         (INT*)    ... ADDRESS OF OTHER ENDPOINT
// OUT:
//    (BOND*)   ... ADDRESS OF CORRESPONDING BOND IN BONDLIST
///////////////////////
Bond* Bond::findBond(Bond* bondlist, int length, int* x1, int* x2) {
  for (int j = 0; j < length; j++) {
    if ((bondlist[j].x1 == x1 && bondlist[j].x2 == x2) ||
        (bondlist[j].x1 == x2 && bondlist[j].x2 == x1)) {
      //std::cout << "found at index " << j << std::endl;
      return &bondlist[j];
    }
  }
  std::cout << "no bond found" << std::endl;
  Bond* b_error = new Bond(0, 0);
  return b_error;
};
