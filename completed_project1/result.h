/////////////////////////////////////////
/// RESULT.H
/// ============
/// STRUCT FOR STORING ALL OBSERVABLES IN ONE PLACE 
/////////////////////////////////////////

#ifndef RESULT_H
#define RESULT_H

#include "observable.h"

struct Result {
  double j;
  Observable Energy;
  Observable Magnetization;
  Observable CV;
};

#endif
