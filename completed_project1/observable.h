/////////////////////////////////////////
/// OBSERVABLE.H
/// ============
/// STRUCT FOR STORING THE MEAN VALUE AND THE UNCERTAINTY 
/// OF A MEASUREMENT IN ONE VARIABLE
/////////////////////////////////////////

#ifndef OBSERVABLE_H
#define OBSERVABLE_H

struct Observable {
  /* mean value of a measurement */
  double val;
  /* uncertainty of a measurement */
  double dev;
};

#endif
