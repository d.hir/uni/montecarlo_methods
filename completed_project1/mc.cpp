///////////////////////////////////////
/// MC.CPP
/// ======
/// IMPLEMENTATION OF MC.H
/// CLASS FOR SIMULATING A Q-STATE POTTS MODEL USING A MARKOV CHAIN MONTE CARLO
/// METROPOLIS ALGORITHM
//////////////////////////////////////

#include "mc.h"

#include <iostream>
/* for writing to stdout */
#include <fstream>
/* for writing to files */
#include <time.h>
/* for getting time for generating foldername */
#include <cstdlib>
/* for sprintf */
#include <cmath>
/* for exp, fmod */
#include "rg.h"
/* for randomly generated numbers */
#include "helper.h"
/* for helper functions */
#include "settings.h"
/* for accessing constants and settings */
#include "observable.h"

///////////////////////
// POTTSMONTECARLO::POTTSMONTECARLO
// ------------
// CONSTRUCTOR FOR GENERATING A MONTE CARLO SIMULATION
//
// IN:
//    DIM1_   (INT)   ... NUMBER OF SITES IN X-DIRECTION
//    DIM2_   (INT)   ... NUMBER OF SITES IN Y-DIRECTION
//    Q_      (INT)   ... NUMBER OF POSSIBLE SPIN STATES
// OUT:
//    -
///////////////////////
PottsMonteCarlo::PottsMonteCarlo(int dim1_, int dim2_, int q_) {
  /* assign values to members */
  this->dim1 = dim1_;
  this->dim2 = dim2_;
  this->q = q_;
  this->N = this->dim1 * this->dim2;
  /* declare lattice S of size N = dim1 * dim2 */
  this->S = new int[this->N];
  
  /* create random generator */
  this->rg = new RandomGenerator(0, this->q - 1, 0.0, 1.0);
};

///////////////////////
// POTTSMONTECARLO::~POTTSMONTECARLO
// ------------
// DESTRUCTOR FOR FREE-ING ALLOCATED SPACE ON HEAP
//
// IN:
//    -
// OUT:
//    -
///////////////////////
PottsMonteCarlo::~PottsMonteCarlo() {
  delete this->S;
  delete this->neib;
  delete this->rg;
}

///////////////////////
// POTTSMONTECARLO::INITIALIZESPINS
// ------------
// INITIALIZE ALL SPINS ON THE LATTICE
//
// IN:
//    HOTSTART    (BOOL)  ... USE RANDOM STATE FOR EACH SPIN
// OUT:
//    -
///////////////////////
void PottsMonteCarlo::initializeSpins(bool hotstart) {
  if (hotstart) {
    /* set each spin to a random state */
    for (int j = 0; j < this->N; j++)
      this->S[j] = this->rg->randn();
  } else {
    /* set all spins to the same random state */
    int random_spin = this->rg->randn();
    for (int j = 0; j < this->N; j++)
      this->S[j] = random_spin;
  }
};

///////////////////////
// POTTSMONTECARLO::PRINTS
// ------------
// PRINT THE CURRENT CONFIGURATION S TO STDOUT
//
// IN:
//    -
// OUT:
//    -
///////////////////////
void PottsMonteCarlo::printS() {
  for (int j = 0; j < this->dim1; j++) {
    for (int k = 0; k < this->dim2; k++)
      std::cout << " " << this->S[j * dim2 + k];
    std::cout << std::endl;
  }
};

///////////////////////
// POTTSMONTECARLO::INITIALIZENEIGHBOURARRAY
// ------------
// INITIALIZE ARRAY TO STORE INDICES OF ALL NEIGHBOURS OF EACH SITE
//
// IN:
//    -
// OUT:
//    -
///////////////////////
void PottsMonteCarlo::initializeNeighbourArray() {
  this->neib = new int[this->N * 4];
  int n1p;
  int n2p;
  int n1m;
  int n2m;
  int s_index;
  for (int n1 = 0; n1 < this->dim1; n1++) {
    n1p = n1 + 1;
    if (n1p == this->dim1)
      n1p = 0;
    n1m = n1 - 1;
    if (n1m == -1)
      n1m = this->dim1 - 1;
    for (int n2 = 0; n2 < this->dim2; n2++) {
      n2p = n2 + 1;
      if (n2p == this->dim2)
        n2p = 0;
      n2m = n2 - 1;
      if (n2m == -1)
        n2m = this->dim2 - 1;
      s_index = n1 + n2 * this->dim1;
      this->neib[4 * s_index]     = n1p + n2*this->dim1; // (n1 + 1, n2    )
      this->neib[4 * s_index + 1] = n1 + n2p*this->dim1; // (n1    , n2 + 1)
      this->neib[4 * s_index + 2] = n1m + n2*this->dim1; // (n1 - 1, n2    )
      this->neib[4 * s_index + 3] = n1 + n2m*this->dim1; // (n1    , n2 - 1)
    }
  }
};

///////////////////////
// POTTSMONTECARLO::SETENERGYPARAMETER
// ------------
// SET J AND M FOR THIS SIMULATION
//
// IN:
//    J   (DOUBLE) ... VALUE FOR NEIGHBOUR COUPLING CONSTANT
//    M   (DOUBLE) ... VALUE FOR MAGNETIZATION COUPLING CONSTANT
// OUT:
//    -
///////////////////////
void PottsMonteCarlo::setEnergyParameter(double J_, double M_, int Mdir_) {
  this->J = J_;
  this->M = M_;
  this->Mdir = Mdir_;
};

///////////////////////
// POTTSMONTECARLO::METROPOLISSTEP
// ------------
// ALGORITHM FOR DOING A SINGLE METROPOLIS STEP AT SITE N0
//
// IN:
//    N0    (INT) ... INDEX OF SITE WHERE TO PERFORM METROPOLIS STEP
// OUT:
//    -
///////////////////////
void PottsMonteCarlo::metropolisStep(int n0) {
  /* propose a random spin state */
  int sigma = this->rg->randn();
  
  /* calculate J-part of change of Hamilton */
  double Jsum = 0;
  for (int nu = 0; nu < 4; nu++) {
    /* add contribution of sites in all directions */
    Jsum += Helper::delta(sigma, this->S[this->neib[4 * n0 + nu]]);
    Jsum -= Helper::delta(this->S[n0], this->S[this->neib[4 * n0 + nu]]);
  }
  /* calculate M-part of change of Hamilton */
  double Msum = Helper::delta(sigma, 1);
  Msum -= Helper::delta(this->S[n0], 1);
  /* calculate transition probability */
  double rho = exp(this->J * Jsum + this->M * Msum);
  
  /* check if transition occurs and perform it if it does */
  double r = this->rg->rand();
  if (r < rho)
    this->S[n0] = sigma;
};

///////////////////////
// POTTSMONTECARLO::SWEEP
// ------------
// PERFORM A METROPOLIS STEP AT EACH SITE OF THE LATTICE EXACTLY N TIMES
//
// IN:
//    N   (INT) ... NUMBER OF SWEEPS TO PERFORM
// OUT:
//    -
///////////////////////
void PottsMonteCarlo::sweep(int n) {
  for (int sweep_number = 0; sweep_number < n; sweep_number++) {
    for (int n0 = 0; n0 < this->N; n0++) {
      this->metropolisStep(n0);
    }    
  }
};

///////////////////////
// POTTSMONTECARLO::GETENERGY
// ------------
// CALCULATE THE ENERGY OF THE CURRENT CONFIGURATION
//
// IN:
//    -
// OUT:
//    (DOUBLE) VALUE FOR THE ENERGY OF THE SYSTEM IN CURRENT STATE
///////////////////////
double PottsMonteCarlo::getEnergy() {
  double sum = 0;
  for (int n0 = 0; n0 < this->N; n0++) {
    for (int nu = 0; nu < 2; nu++) {
      sum -= Helper::delta(this->S[n0], this->S[this->neib[4 * n0 + nu]]);
    }
  }
  sum /= this->N;
  sum -= this->getMagnetization(this->Mdir) * this->M / this->J;
  return sum;
};

///////////////////////
// POTTSMONTECARLO::GETMAGNETIZATION
// ------------
// CALCULATE THE MAGNETIZATION OF THE CURRENT CONFIGURATION
//
// IN:
//    -
// OUT:
//    (DOUBLE) VALUE FOR THE MAGNETIZATION OF THE SYSTEM IN CURRENT STATE
///////////////////////
double PottsMonteCarlo::getMagnetization(int dir) {
  double sum = 0;
  for (int n0 = 0; n0 < this->N; n0++) {
    sum += Helper::delta(this->S[n0], dir);
  }
  return sum / this->N;
};

///////////////////////
// POTTSMONTECARLO::STOREDATA
// ------------
// STORE CURRENT CONFIGURATION IN *.CSV FILE
//
// IN:
//    FOLDERNAME_LENGTH   (INT)   ... LENGTH OF NAME OF FOLDER
//    FOLDERNAME          (CHAR*) ... NAME OF FOLDER WHERE TO STORE DATA
// OUT:
//    -
///////////////////////
void PottsMonteCarlo::storeData(int foldername_length, char* foldername) {
  std::ofstream file;
  char filepath[2 * foldername_length];
  char jstr[6];
  char sign = 'p';
  double absj = this->J;
  if (this->J < 0) {
    sign = 'm';
    absj = -absj;
  }  
  sprintf(jstr, "%c%1.2f", sign, absj);
  sprintf(filepath, "%s/S_J%s.csv", foldername, jstr);
  file.open(filepath);
  if (!file.is_open()) { 
    std::cerr << "ERROR: Could not open file";
    return;
  }
  for (int row = 0; row < dim2; row++) {
    for (int col = 0; col < dim1 - 1; col++)
      file << S[row * dim1 + col] << ",";
    file << S[(row + 1) * dim1 - 1] << "\n";
  }
  file.close();
};

///////////////////////
// POTTSMONTECARLO::GETJC
// ------------
// GET THE CRITICAL COUPLING FOR THE CURRENT SETUP
//
// IN:
//    -
// OUT:
//    (DOUBLE) CRITICAL COUPLING ROUNDED TO MATCH PRECISION OF 'J_STEP'
///////////////////////
double PottsMonteCarlo::getJc(int q) {
  double jc = log(1 + sqrt(q));
  /* round to match precision of J_STEP */
  jc -= fmod(jc, J_STEP);
  return jc;
};


