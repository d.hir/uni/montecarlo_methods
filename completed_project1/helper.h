///////////////////////////////////////
/// HELPER.H
/// ========
/// STATIC CLASS WHICH ACTS AS COLLECTION OF VARIOUS FUNCTIONS
///
/// CANNOT BE INSTANTIATED!
///////////////////////////////////////

#ifndef HELPER_H
#define HELPER_H

#include "observable.h"
/* for using the 'Observable' struct */
#include "settings.h"
/* for accessing constants and settings */
#include "result.h"

class Helper {
  private:
    /* private constructor => cannot create instance of this class */
    Helper() {};
    
  public:
    /* calculate mean value of array 'A' with length 'length' */
    static double mean (double* A, int length);
    /* calculate A[i] * A[i + shift]. Result is stored at 'res' */
    static void shiftsquare (double* res, double* A, int length, int shift);
    /* calculate uncertainty of 'x' of length 'nmeas' using autocorrelation */
    static double getAutocorrelation (double* x, int nmeas);
    /* calculate mean value omitting index 'exception' */
    static double meanExcept(double* x, int length, int exception);
    /* calculate the uncertainty using Jackknife method */
    static double getGeneralJackknifeDev(double (*ObservableFunctionExcept)
        (double*, int, int), double* x, int length);
    /* print Observable to stdout */
    static void printResults(Result res[J_NUM], int length);
    /* store mean and deviation of 'O_vec' in observable 'O' */
    static void setObservable(Observable &O, double* O_vec, int nmeas);
    /* store results from 'R' in './foldername/observables.csv' */
    static void storeObservables(Result* R, int num_vals, 
        int foldername_length, char* foldername);
    /* parse 'argv' and store values in the given variables */
    static void parseInputs(int argc, char** argv, int &dimx, int &dimy, 
        int &q, int &nequi, int &nmeas, int &nskip, bool &hotstart, 
        long &run_uid, char foldername[STRING_BUFFER_LENGTH]);
    /* kronecker delta function */
    static int delta(const int a, const int b);
    /* calculate the heat capacity omitting index 'exception' */
    static double CVExcept(double* energy, int length, int exception);
    /* calculate mean value and uncertainty of heat capacity */
    static void setHeatCapacity(Result &R, double* energy, int length);
};

#endif
