///////////////////////////////////////
/// MC.H
/// ====
/// CLASS FOR SIMULATING A Q-STATE POTTS MODEL USING A MARKOV CHAIN MONTE CARLO
/// METROPOLIS ALGORITHM
//////////////////////////////////////

#ifndef MC_H
#define MC_H

#include "rg.h"
/* for using random variables */
#include "observable.h"

class PottsMonteCarlo {
  private: 
    int dim1 = 4;         // number of sites in x-direction
    int dim2 = 4;         // number of sites in y-direction
    int N = 16;           // number of total sites (= dim1 * dim2)
    int q = 2;            // number of possible spin states
    int* S;               // pointer to lattice of spins
    int* neib;            // dim1*dim2*4 array for storing neighbouring sites
    double J = 1;         // coupling to neighbouring spins
    double M = 0;         // coupling to external magnetic field
    int Mdir = 1;         // direction of external magnetic field
    RandomGenerator* rg;  // random generator for creating random numbers
  
  public:
    /* create a simulation of size dim1_ * dim2_ and q_ possible spin states */
    PottsMonteCarlo(int dim1_, int dim2_, int q_);
    /* destructor for free-ing allocated space on heap */
    ~PottsMonteCarlo();
    /* initialize spins (hotstart: randomly, else uniformly) */
    void initializeSpins(bool hotstart);
    /* print lattice to stdout, used mainly for debugging */
    void printS();
    /* initialize array of indices of neighbouring sites */
    void initializeNeighbourArray();
    /* set parameters of J and M for this simulation */
    void setEnergyParameter(double J_, double M_, int Mdir_);
    
    /* do a single metropolis step at site n0 */
    void metropolisStep(int n0);
    /* sweep n times through the whole lattice */
    void sweep(int n);
    /* get the energy of the current configuration */
    double getEnergy();
    /* get the magnetization of the current configuration in direction 'dir' */
    double getMagnetization(int dir);
    /* save current configuration as *.csv file in directory 'foldername' */
    void storeData(int foldername_length, char* foldername);
    /* get the critical J value for current setup */
    static double getJc(int q);
};

#endif
