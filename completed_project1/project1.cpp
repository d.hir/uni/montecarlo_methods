///////////////////////////
/// PROJECT1.CPP
/// ============
/// MAIN FILE FOR RUNNING THE MARKOV CHAIN MONTE CARLO ALGORITHM.
/// 
/// INPUT ARGUMENTS:
///   'RUN_UID'   (STRING) ... UID FOR THIS RUN
///   'DIM_X'     (INT)    ... SITES IN X-DIMENSION
///   'DIM_Y'     (INT)    ... SITES IN Y-DIMENSION
///   'Q'         (INT)    ... NUMBER OF POSSIBLE SPINS
///   'NEQUI'     (INT)    ... NUMBER OF EQUILIBRATION SWEEPS
///   'NMEAS'     (INT)    ... NUMBER OF MEASUREMENTS PER RESULT
///   'NSKIP'     (INT)    ... NUMBER OF SWEEPS BETWEEN MEASUREMENTS
///   'HOTSTART'  (BOOL)   ... RANDOMIZE STARTING CONFIGURATION (0/1)
//////////////////////////


#include "mc.h"
/* for the PottsMonteCarlo class, the heart of this program */
#include "settings.h"
/* for the default values and hardcoded constants */
#include "helper.h"
/* for various functions not related directly to specific MC run*/
#include "observable.h"
/* for usage of the Observable struct */
#include <iostream>
/* for printing stuff to stdout */
#include <cmath>
/* for basic mathematical functions */
#include "result.h"
#include <future>

/* declare doSimulation function which is implemented further down */
Result doSimulation(double J, int dimx, int dimy, int q, int nequi, 
    int nmeas, int nskip, bool hotstart, long run_uid, 
    char foldername[STRING_BUFFER_LENGTH]);

int main(int argc, char** argv)
{
  /* setup default values from 'settings.h' */
  int dimx = DEFAULT_DIM_X;
  int dimy = DEFAULT_DIM_Y;
  int q = DEFAULT_Q_VAL;
  int nequi = DEFAULT_N_EQUI;
  int nmeas = DEFAULT_N_MEAS;
  int nskip = DEFAULT_N_SKIP;
  bool hotstart = DEFAULT_USE_HOTSTART;
  
  /* parse inputs given through commandline */
  long run_uid = 0;
  char foldername[STRING_BUFFER_LENGTH];
  Helper::parseInputs(argc, argv, dimx, dimy, q, nequi, nmeas, nskip, 
      hotstart, run_uid, foldername);

  
  
  /* estimate appropriate values for J */
  double j_min = PottsMonteCarlo::getJc(q) - floor(0.5 * J_NUM) * J_STEP;
  double j_max = j_min + J_STEP * (J_NUM - 1);
  
  /* print simulation parameters to stdout */
  std::cout << "nequi= " << nequi << std::endl;
  std::cout << "nmeas= " << nmeas << std::endl;
  std::cout << "nskip= " << nskip << std::endl;
  std::cout << "jc= " << PottsMonteCarlo::getJc(q) << std::endl;
  std::cout << "foldername= " << foldername+2 << std::endl;
  std::cout << "J E Edev M Mdev C Cdev" << std::endl;
  
  /* define arrays for final results of measurements of observables. */
  /* the std::future handle is used for parallelization */
  Result R[J_NUM];
  std::future<Result> R_handle[J_NUM];
  
  //**  ACTUAL MCMC METROPOLIS ALGORITHM  **//
  /* loop over all values of J and call 'doSimulation' asynchronously */
  int index = 0;
  for (double J = j_min; J <= j_max + 1e-2; J += J_STEP) {
    R_handle[index] = std::async(std::launch::async, doSimulation, J, dimx, 
        dimy, q, nequi, nmeas, nskip, hotstart, run_uid, foldername);
    index++;
  }
  
  /* get the results of all parallel computations */
  /* here the program will wait for all invoked threads to finish */
  for (int k = 0; k < J_NUM; k++)
    R[k] = R_handle[k].get();
  
  /* print all results to stdout */
  Helper::printResults(R, J_NUM);
  
  /* store results of measurements as *.csv file in own directory */
  Helper::storeObservables(R, J_NUM, STRING_BUFFER_LENGTH, foldername);
  
  return 0;  
}

///////////////////////
// DOSIMULATION
// ------------
// DO THE MONTECARLO SIMULATION FOR A SPECIFIC VALUE FOR J
//
// IN:
//   'J'          (DOUBLE) ... ENERGY PARAMETER J
//   'DIM_X'      (INT)    ... SITES IN X-DIMENSION
//   'DIM_Y'      (INT)    ... SITES IN Y-DIMENSION
//   'Q'          (INT)    ... NUMBER OF POSSIBLE SPINS
//   'NEQUI'      (INT)    ... NUMBER OF EQUILIBRATION SWEEPS
//   'NMEAS'      (INT)    ... NUMBER OF MEASUREMENTS PER RESULT
//   'NSKIP'      (INT)    ... NUMBER OF SWEEPS BETWEEN MEASUREMENTS
//   'HOTSTART'   (BOOL)   ... RANDOMIZE STARTING CONFIGURATION (0/1)
//   'RUN_UID'    (LONG)   ... UID FOR THIS RUN
//   'FOLDERNAME' (STRING) ... NAME OF THE FOLDER WHERE RESULTS ARE STORED
// OUT:
//    (RESULT) STRUCT CONTAINING ALL OBSERVABLES WITH UNCERTAINTIES
///////////////////////
Result doSimulation(double J, int dimx, int dimy, int q, int nequi, int nmeas, int nskip, bool hotstart, long run_uid, char foldername[STRING_BUFFER_LENGTH]) {
  
  /* set magnetization to 0 */
  double M = 0;
  
  /* create PottsMonteCarlo object and initialize lattice */
  PottsMonteCarlo pmc (dimx, dimy, q);
  pmc.initializeSpins(hotstart);
  pmc.initializeNeighbourArray();
  Result R;
  
  /* create array for storing single measurement results */
  double energy[nmeas];
  double magnetization[nmeas];
  
  /* set energy and magnetization parameters for this run */
  pmc.setEnergyParameter(J, M, MAGNETIC_FIELD_DIRECTION);
  /* store used value of J */
  R.j = J;
  /* do 'nequi' equilibration sweeps */
  pmc.sweep(nequi);
  
  /* do nmeas measurements of observable */
  for (int i = 0; i < nmeas; i++) {
    /* sweep 'nskip' times without measuring */
    pmc.sweep(nskip + 1);
    /* measure the energy and the magnetization of current configuration */
    energy[i] = pmc.getEnergy(); 
    magnetization[i] = pmc.getMagnetization(1);
  }
  
  /* calculate mean and deviation for observables and store them in R */
  Helper::setObservable(R.Energy, energy, nmeas);
  Helper::setObservable(R.Magnetization, magnetization, nmeas);
  Helper::setHeatCapacity(R, energy, nmeas);
  
  /* store last lattice configuration as *.csv file in own directory */
  pmc.storeData(STRING_BUFFER_LENGTH, foldername);
  
  /* return result struct R */
  return R;
}
