#!/usr/bin/env python3

import subprocess
## for calling C++ program
import numpy
## for numpy arrays

import matplotlib
matplotlib.use('TkAgg');
## change backend to fix warnings in suppressed mode

import matplotlib.pyplot as mpl
import matplotlib.animation as animation
from matplotlib.widgets import Slider
## for plotting results
import datetime
## for generating the uid
import argparse
## for parsing the commandline arguments


#### Parsing the commandline arguments
parser = argparse.ArgumentParser();
parser.add_argument('-c', '--compile', action='store_true', 
    help="recompile C++ program");
parser.add_argument('-r', '--remove', action='store_true', 
    help="remove all directories created for storing results");
parser.add_argument('-s', '--suppress', action='store_true', 
    help="don't show plots after computation");
parser.add_argument('-t', '--temporary', action='store_true', 
    help="delete all files created during computation once finished");
parser.add_argument('-m', '--make', action='store_true', 
    help="just compile C++ program and remove all directories");
parser.add_argument('-d', '--default', action='store_true', 
    help="use default parameters instead of the ones defined in this script");
parser.add_argument('-g', '--gif', action='store_true',
    help="create a gif file animating the lattice over time");
parser.add_argument('--dimx', default=8, 
    help="number of sites in x-direction (default: 8)");
parser.add_argument('--dimy', default=8, 
    help="number of sites in y-direction (default: 8)");
parser.add_argument('--q', default=2,
    help="number of possible different spins (default: 2)");
parser.add_argument('--runid', default=0,
    help="id of this run (default: time of execution)");
args = parser.parse_args();

#### compile the C++ files if asked for
if args.compile or args.make:
  print("Compiling...");
  ret = subprocess.call("g++ *.cpp -o project1 -Wall -pthread", shell=True);
  if not ret == 0:
    print('Return value: ' + ret);
    exit(1);

#### remove all subdirectories if asked for
if args.remove or args.make:
  print("Removing previous results...");
  subprocess.call('rm -rf */', shell=True);

#### exit if only compilation and directory removal was ordered
if args.make:
  exit();

#### generate UID based on time
if not args.runid:
  dt = datetime.datetime.now()
  run_uid = "{:04d}{:02d}{:02d}{:02d}{:02d}{:02d}".format(dt.year, 
      dt.month, dt.day, dt.hour, dt.minute, dt.second);
else:
  run_uid = str(args.runid);
print("ID = " + run_uid);

#### define simulation parameters
dimx = args.dimx;     # (int) number of sites in x direction
dimy = args.dimy;     # (int) number of sites in y direction
q = args.q;        # (int) number of possible states 
nequi = 100;  # (int) number of equilibration sweeps
nmeas = 100;   # (int) number of measurements per result
nskip = 10;    # (int) number of sweeps between measurements
hotstart = 1; # (bool as int) randomize each site at beginning

#### create array of arguments for passing to C++ program
cppargs = [str(run_uid)];
if not args.default:
  cppargs += [str(dimx), str(dimy), str(q), str(nequi), 
    str(nmeas), str(nskip), str(hotstart)];

#### call the C++ program, capture the output from stdout and
#### split each line of the output in separate entries in matrix 'res'
print("Starting Simulation...");
processHandle = subprocess.run(["./project1"] + cppargs, capture_output=True)
res = [j.split() for j in processHandle.stdout.splitlines()];
if processHandle.stderr:
  errmsg = processHandle.stderr.splitlines();
  print('\n'.join(['<STDERR:> ' + str(j)[2:-1] for j in errmsg]));

#### extract parameters returned from C++ program
#### this is just used for confirming correct parameters
print("Parsing results...");
nequi = int(res[0][1]);
nmeas = int(res[1][1]);
nskip = int(res[2][1]);
jc = float(res[3][1]);  # critical temperature in form of parameter J
dirname = str(res[4][1])[2:-1];

#### extract name of calculated observables
observable_names = [str(j)[2:-1] for j in res[5]];

#### convert values in string list 'res' to useful numpy.array datatype
data = numpy.array(res[6:], dtype = float).transpose();

#### all values of J are printed first by C++ program
J = data[0];

#### plot all observables with uncertainty and a line at jc and save plots
print("Plotting observables...");
for j in range(1, len(observable_names), 2):
  mpl.figure(figsize=(6, 9));
  mpl.errorbar(J, data[j], data[j + 1], ls='');
  mpl.plot([jc, jc], [min(data[j] - data[j + 1]), max(data[j] + 
      data[j + 1])], 'm--', linewidth = .5);
  mpl.xlabel('J');
  mpl.ylabel(observable_names[j] + "(J)");
  mpl.title(observable_names[j] + "(J)");
  mpl.savefig(dirname + "/" + observable_names[j] + ".jpg");

#### read files stored by C++ program and store results in list
#### each file contains a spin configuration at the specified J value
#### at the end the list gets converted to numpy.array

if args.gif or not args.suppress:
  print("Loading result files...");
  tmp_data = []
  for j in J:
    strj = 'p';
    if j < 0:
      strj = 'm';
    strj += "{:1.2f}".format(abs(j));
    with open(dirname + "/S_J" + strj + ".csv") as file:
      tmp_data.append([j[:-1].split(',') for j in file.readlines()]);
  resmat = numpy.array(tmp_data, dtype=int);

#### create figure for spin-configurations with a slider for changing J
if not args.suppress:
  print("Plotting lattice with slider...");
  fig = mpl.figure(figsize=(5, 6));
  ax = mpl.subplot(111);
  fig.subplots_adjust(bottom=0.25);
  img = ax.matshow(resmat[0]);
  ax = fig.add_axes([0.15, 0.1, 0.65, 0.03])
  dJ = J[1] - J[0];
  slider = Slider(ax, 'J', J[0], J[-1], valinit=jc, valfmt='%1.2f', valstep=dJ)
  def update(val):
    wantedj = float(slider.val);
    indexj = int((wantedj - J[0]) / dJ);
    img.set_data(resmat[indexj]);
    fig.canvas.draw();
  slider.on_changed(update);

#### create figure for plotting the spin-configurations as a looping gif
if args.gif:
  print("Creating animated gif...");
  giffig = mpl.figure(figsize=(5, 5));
  images = [];
  for j in range(len(J)):
    tmp_img = mpl.imshow(resmat[j]);
    images.append([tmp_img]);
  ani = animation.ArtistAnimation(giffig, images, interval=3e4/len(J), blit=False);
  ani.save(dirname + "/S.gif", writer='imagemagick');

#### remove directory created in this run
if args.temporary:
  print("Removing traces...");
  subprocess.call('rm -rf ' + dirname + '/', shell=True);
  
#### show plots if not explicitly asked to suppress the output
if not args.suppress:
  mpl.show();

