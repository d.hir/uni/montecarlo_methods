#!/usr/bin/env python3

import subprocess
import datetime

ret = subprocess.call(['./mcwrapper.py', '--make']);
if ret: exit(ret);

dt = datetime.datetime.now();
runid = "{:04d}{:02d}{:02d}{:02d}{:02d}{:02d}".format(dt.year, 
    dt.month, dt.day, dt.hour, dt.minute, dt.second);
print("Generated ID: " + runid);
 
q = ['2', '4', '8', '20'];
dim = ['8', '16', '32'];
dim = ['32'];

qlist = [];
dimlist = [];

for dimelem in dim:
  for qelem in q:
    qlist.append(qelem);
    dimlist.append(dimelem);

for j in range(len(qlist)):
  print("Running Simulation for q = " + qlist[j] + " at lattice " + 
      dimlist[j] + "x" + dimlist[j] + "...");
  args = ['--suppress', '--runid', runid,
      '--q', qlist[j], '--dimx', dimlist[j], '--dimy', dimlist[j]];
  subprocess.call(["./mcwrapper.py"] + args);
