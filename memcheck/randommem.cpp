#include "../project/rg.h"
#include <iostream>

int main() {
  RandomGenerator rg (0, 1, 0.0, 1.0);
  std::cout << rg.rand() << std::endl;
  return 0;
}
