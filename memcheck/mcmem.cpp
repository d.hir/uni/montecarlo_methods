#include "../project/mc.h"
#include "../project/ising.h"


int main() {
  PottsMonteCarlo* mc = new Ising(4, 4);
  mc->initializeSpins(true);
  mc->setEnergyParameter(0.5, 0.0, 1);
  
  mc->sweep(1);
  mc->getEnergy();
  mc->getMagnetization(1);
  char f[8] = "testdir";
  mc->storeData(6, &f[0]);
  
  delete mc;
  return 0;
}
